/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class ConfigReader {

    private static ConfigReader ins = null;

    private ConfigReader() {

    }

    public static ConfigReader getIns() {
        if (ins == null) {
            ins = new ConfigReader();
        }

        return ins;
    }

    public boolean read() {
        String path = Paths.get("").toAbsolutePath().toString().replace("\\", "/") + "/" + CommonValue.PROP_FILE_NAME;
        Properties prop = new Properties();
        try {
            InputStream inputStream = new FileInputStream(path);
            if (inputStream != null) {
                prop.load(inputStream);
            }
            if (prop.size() == 0) {
                return false;
            }
            CommonValue.DB_HOST = prop.getProperty("DB_HOST");
            CommonValue.DB_DBNAME = prop.getProperty("DB_DBNAME");
            CommonValue.DB_PORT = prop.getProperty("DB_PORT");
            CommonValue.DB_USERNAME = prop.getProperty("DB_USERNAME");
            CommonValue.DB_PASSWORD = prop.getProperty("DB_PASSWORD");
            CommonValue.THESIS_REPO_PATH = prop.getProperty("THESIS_REPO_PATH");
            CommonValue.THESIS_PREPROCESS_PATH = prop.getProperty("THESIS_PREPROCESS_PATH");
            CommonValue.OUTPUT_REPO_PATH = prop.getProperty("OUTPUT_REPO_PATH");
            CommonValue.SHOW_REPO_PATH = prop.getProperty("SHOW_REPO_PATH");
            CommonValue.SENTENCE_CHANGE_THRESHOLE = Double.parseDouble(prop.getProperty("SENTENCE_CHANGE_THRESHOLE"));
            CommonValue.THESIS_CHANGE_THRESHOLE = Double.parseDouble(prop.getProperty("THESIS_CHANGE_THRESHOLE"));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfigReader.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ConfigReader.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        ConfigReader configReader = ConfigReader.getIns();
        configReader.read();

        System.out.println(CommonValue.DB_HOST);
        System.out.println(CommonValue.DB_DBNAME);
        System.out.println(CommonValue.DB_PORT);
        System.out.println(CommonValue.DB_USERNAME);
        System.out.println(CommonValue.DB_PASSWORD);
        System.out.println(CommonValue.THESIS_REPO_PATH);
        System.out.println(CommonValue.OUTPUT_REPO_PATH);
        System.out.println(CommonValue.THESIS_PREPROCESS_PATH);
        System.out.println(CommonValue.SHOW_REPO_PATH);
        System.out.println(CommonValue.SENTENCE_CHANGE_THRESHOLE);
        System.out.println(CommonValue.THESIS_CHANGE_THRESHOLE);
    }
}
