/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.config;

/**
 *
 * @author User
 */
public class CommonValue {
    
    public static String PROP_FILE_NAME = "config.properties";
    
    public static String DEFAULT_DB_HOST = "localhost";
    public static String DEFAULT_DB_DBNAME = "test";
    public static String DEFAULT_DB_PORT = "3306";
    public static String DEFAULT_DB_USERNAME = "root";
    public static String DEFAULT_DB_PASSWORD = "";
    
    public static String DEFAULT_THESIS_REPO_PATH = "E:/Temp/Thesis/ConHD/Data/Raw";
    public static String DEFAULT_THESIS_PREPROCESS_PATH = "E:/Temp/Thesis/ConHD/Data/PreProcess";
    public static String DEFAULT_OUTPUT_REPO_PATH = "E:/Temp/Thesis/ConHD/Data/Final";
    public static String DEFAULT_SHOW_REPO_PATH = "E:/Temp/Thesis/ConHD/Data/Show";
    
    public static String DEFAULT_SENTENCE_CHANGE_THRESHOLE = "0.4";
    public static String DEFAULT_THESIS_CHANGE_THRESHOLE = "0.2";
    
    public static String DB_HOST = null;
    public static String DB_DBNAME = null;
    public static String DB_PORT = null;
    public static String DB_USERNAME = null;
    public static String DB_PASSWORD = null;
    
    public static String THESIS_REPO_PATH = null;
    public static String THESIS_PREPROCESS_PATH = null;
    public static String OUTPUT_REPO_PATH = null;
    public static String SHOW_REPO_PATH = null;
    
    public static double SENTENCE_CHANGE_THRESHOLE = 0.4;
    public static double THESIS_CHANGE_THRESHOLE = 0.2;
    
}
