/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author User
 */
public class ConfigBuilder {

    private static ConfigBuilder ins = null;

    private ConfigBuilder() {

    }

    public static ConfigBuilder getIns() {
        if (ins == null) {
            ins = new ConfigBuilder();
        }

        return ins;
    }

    public boolean isExist() {
        File file;

        String path = Paths.get("").toAbsolutePath().toString().replace("\\", "/") + "/" + CommonValue.PROP_FILE_NAME;
        System.out.println(path);
        file = new File(path);

        return (file.exists());

    }

    public boolean build() {

        if (isExist()) {
            return false;
        }

        try {
//            File file = new File(getClass().getClassLoader().getResource("") + CommonValue.PROP_FILE_NAME);
            File file = new File(Paths.get("").toAbsolutePath().toString().replace("\\", "/") + "/" + CommonValue.PROP_FILE_NAME);
            if (!file.createNewFile()) {
                return false;
            }
            OutputStream outputStream = new FileOutputStream(file);
            Properties prop = new Properties();

            prop.setProperty("DB_HOST", CommonValue.DEFAULT_DB_HOST);
            prop.setProperty("DB_DBNAME", CommonValue.DEFAULT_DB_DBNAME);
            prop.setProperty("DB_PORT", CommonValue.DEFAULT_DB_PORT);
            prop.setProperty("DB_USERNAME", CommonValue.DEFAULT_DB_USERNAME);
            prop.setProperty("DB_PASSWORD", CommonValue.DEFAULT_DB_PASSWORD);

            prop.setProperty("THESIS_REPO_PATH", CommonValue.DEFAULT_THESIS_REPO_PATH);
            prop.setProperty("THESIS_PREPROCESS_PATH", CommonValue.DEFAULT_THESIS_PREPROCESS_PATH);
            prop.setProperty("OUTPUT_REPO_PATH", CommonValue.DEFAULT_OUTPUT_REPO_PATH);
            prop.setProperty("SHOW_REPO_PATH", CommonValue.DEFAULT_SHOW_REPO_PATH);

            prop.setProperty("SENTENCE_CHANGE_THRESHOLE", CommonValue.DEFAULT_SENTENCE_CHANGE_THRESHOLE);
            prop.setProperty("THESIS_CHANGE_THRESHOLE", CommonValue.DEFAULT_THESIS_CHANGE_THRESHOLE);

            prop.store(outputStream, null);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfigBuilder.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ConfigBuilder.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    public boolean update() {
        if (!isExist()) {
            return false;
        }

        String path = Paths.get("").toAbsolutePath().toString().replace("\\", "/") + "/" + CommonValue.PROP_FILE_NAME;
        PropertiesConfiguration configuration = new PropertiesConfiguration();

        if (CommonValue.DB_HOST == null
                || CommonValue.DB_DBNAME == null
                || CommonValue.DB_PORT == null
                || CommonValue.DB_USERNAME == null
                || CommonValue.DB_PASSWORD == null
                || CommonValue.THESIS_REPO_PATH == null
                || CommonValue.THESIS_PREPROCESS_PATH == null
                || CommonValue.OUTPUT_REPO_PATH == null
                || CommonValue.SHOW_REPO_PATH == null) {
            return false;
        }

        configuration.setProperty("DB_HOST", CommonValue.DB_HOST);
        configuration.setProperty("DB_DBNAME", CommonValue.DB_DBNAME);
        configuration.setProperty("DB_PORT", CommonValue.DB_PORT);
        configuration.setProperty("DB_USERNAME", CommonValue.DB_USERNAME);
        configuration.setProperty("DB_PASSWORD", CommonValue.DB_PASSWORD);
        
        configuration.setProperty("THESIS_REPO_PATH", CommonValue.THESIS_REPO_PATH);
        configuration.setProperty("THESIS_PREPROCESS_PATH", CommonValue.DEFAULT_THESIS_PREPROCESS_PATH);
        configuration.setProperty("OUTPUT_REPO_PATH", CommonValue.OUTPUT_REPO_PATH);
        configuration.setProperty("SHOW_REPO_PATH", CommonValue.DEFAULT_SHOW_REPO_PATH);
        
        configuration.setProperty("SENTENCE_CHANGE_THRESHOLE", CommonValue.DEFAULT_SENTENCE_CHANGE_THRESHOLE);
        configuration.setProperty("THESIS_CHANGE_THRESHOLE", CommonValue.DEFAULT_THESIS_CHANGE_THRESHOLE);

        try {
            configuration.save();
        } catch (ConfigurationException ex) {
            Logger.getLogger(ConfigBuilder.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    public boolean delete() {
        String path = Paths.get("").toAbsolutePath().toString().replace("\\", "/") + "/" + CommonValue.PROP_FILE_NAME;
        File file;
        file = new File(path);

        return file.delete();
    }

    public boolean rebuild() {
        return (delete() && rebuild());
    }

    public static void main(String[] args) throws FileNotFoundException {
        ConfigBuilder configBuilder = ConfigBuilder.getIns();
        configBuilder.build();
    }
}
