/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dm;

import java.util.Date;

/**
 *
 * @author User
 */
public class Student {

    private String code;
    private String firstname;
    private String surname;
    private String lastname;
    private int year;
    private String aclass;
    private String phonenumber;
    private String email;
    private Date dob;
    private String pob;

    public Student() {
    }

    public Student(String code, String firstname, String surname, String lastname, int year, String aclass, String phonenumber, String email, Date dob, String pob) {
        this.code = code;
        this.firstname = firstname;
        this.surname = surname;
        this.lastname = lastname;
        this.year = year;
        this.aclass = aclass;
        this.phonenumber = phonenumber;
        this.email = email;
        this.dob = dob;
        this.pob = pob;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAclass() {
        return aclass;
    }

    public void setAclass(String aclass) {
        this.aclass = aclass;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    @Override
    public String toString() {
        return firstname + " " + surname + " " + lastname;
    }

}
