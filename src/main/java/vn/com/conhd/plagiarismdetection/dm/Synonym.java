/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dm;

/**
 *
 * @author User
 */
public class Synonym {
    
    private int id;
    private int connotationID;
    private String content;

    public Synonym() {
    }

    public Synonym(int id, int connotationID, String content) {
        this.id = id;
        this.connotationID = connotationID;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConnotationID() {
        return connotationID;
    }

    public void setConnotationID(int connotationID) {
        this.connotationID = connotationID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    
    
}
