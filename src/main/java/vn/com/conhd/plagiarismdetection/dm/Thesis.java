/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dm;

/**
 *
 * @author User
 */
public class Thesis {
    private String code;
    private String decisionCode;
    private String studentCode;
    private int domainID;
    private int year;
    private String title;
    private String summary;

    public Thesis() {
    }

    public Thesis(String code, String decisionCode, String studentCode, int domainID, int year, String title, String summary) {
        this.code = code;
        this.decisionCode = decisionCode;
        this.studentCode = studentCode;
        this.domainID = domainID;
        this.year = year;
        this.title = title;
        this.summary = summary;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDecisionCode() {
        return decisionCode;
    }

    public void setDecisionCode(String decisionCode) {
        this.decisionCode = decisionCode;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public int getDomainID() {
        return domainID;
    }

    public void setDomainID(int domainID) {
        this.domainID = domainID;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
