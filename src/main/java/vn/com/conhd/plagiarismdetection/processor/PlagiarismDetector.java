/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import vn.com.conhd.plagiarismdetection.file.MSWordFile;
import vn.com.conhd.plagiarismdetection.file.MSWordFileHandler;
import vn.com.conhd.plagiarismdetection.preprocessor.ThesisFile;
import vn.com.conhd.plagiarismdetection.ui.FormMain;

/**
 *
 * @author User
 */
public class PlagiarismDetector {

    public static List<Result> detect(ThesisFile thesis, boolean isUsingSemantic) {
        FormMain formMain = FormMain.getIns();
        MSWordFileHandler msWordFileHandler = MSWordFileHandler.getIns();
        msWordFileHandler.loadAll();
        List<MSWordFile> msWordFiles = msWordFileHandler.getMsWordFiles();
        formMain.addLog("Reading these...\n");
        ThesisFile tempThesis;
        Result result;
        String filename1;
        String filename2;
        String[] filenames;
        List<ThesisFile> these = new ArrayList<>();
        for (MSWordFile msWordFile : msWordFiles) {
            tempThesis = new ThesisFile(msWordFile);
            tempThesis.segment();
            these.add(tempThesis);
        }

//        String[] choices = new String[these.size()];
//        for (int i = 0; i < these.size(); i++) {
//            formMain.addLog(these.get(i).getMsWordFile().getPath() + "\n");
//            filename1 = these.get(i).getMsWordFile().getPath().substring(
//                    these.get(i).getMsWordFile().getPath().lastIndexOf("\\") + 1);
//            choices[i] = filename1;
//        }
//        String choice = (String) JOptionPane.showInputDialog(null, "Choose theis",
//                "Choose thesis to detect plagiarism",
//                JOptionPane.QUESTION_MESSAGE,
//                null,
//                choices,
//                choices[1]);
//        
//        int index = -1;
//        for (int i = 0; i < these.size(); i++) {
//            if (these.get(i).getMsWordFile().getPath().contains(choice)) {
//                index = i;
//                break;
//            }
//        }
        List<Result> results = new ArrayList<>();
        for (int i = 0; i < these.size(); i++) {
//            if (i == index) {
//                System.out.println("Bypassing " + i);
//                continue;
//            }
            System.out.println("Comparing " + i);
            result = new Result(thesis, these.get(i));
            result.setIsUsingSemantic(isUsingSemantic);
            result.calculateSimilarity();
            results.add(result);
        }
        Collections.sort(results);
        return results;
    }

    public static void main(String[] args) {

    }
}
