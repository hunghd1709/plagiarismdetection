/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

/**
 *
 * @author User
 */
public class LevenshteinProcessor {
    
    public static double getDistance(String s, String t, boolean isUsingSemantic) {
        SemanticComparator semanticComparator = SemanticComparator.getIns();
        int cost;
        String[] as = s.split(" ");
        String[] at = t.split(" ");
        int m = as.length;
        int n = at.length;
        if (as.length == 0 || at.length == 0) {
            return -1.0;
        }
        
        int[][] d = new int[m + 1][n + 1];
        
        for (int i = 0; i <= m; i++) {
            d[i][0] = i;
        }
        for (int j = 0; j <= n; j++) {
            d[0][j] = j;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (isUsingSemantic) {
                    if (semanticComparator.isSynonym(as[i - 1], at[j - 1])) {
                        cost = 0;
                    } else {
                        cost = 1;
                    }
                } else if (s.equals(t)) {
                    cost = 0;
                } else {
                    cost = 1;
                }
                
                d[i][j] = Math.min(Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1), d[i - 1][j - 1] + cost);
            }
        }
        
        return d[m][n] * 1.0 / m;
        
    }
    
    public static void main(String[] args) {
        System.out.println(getDistance("An tiêm thuốc kháng_sinh", "Bình chích thuốc kháng_sinh", true));
    }
}
