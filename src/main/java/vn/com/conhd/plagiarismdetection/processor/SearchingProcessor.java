/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import vn.com.conhd.plagiarismdetection.config.CommonValue;
import vn.com.conhd.plagiarismdetection.config.ConfigReader;
import vn.com.conhd.plagiarismdetection.file.MSWordFile;
import vn.com.conhd.plagiarismdetection.file.MSWordFileHandler;

/**
 *
 * @author User
 */
public class SearchingProcessor {

    private FSDirectory directoryIndex = null;
    private StandardAnalyzer analyzer = null;
    private static SearchingProcessor ins = null;

    private SearchingProcessor() {

    }

    public static SearchingProcessor getIns() {
        if (ins == null) {
            ins = new SearchingProcessor();
        }
        return ins;
    }

    public Directory getDirectoryIndex() {
        return directoryIndex;
    }

    public void setDirectoryIndex(FSDirectory directoryIndex) {
        this.directoryIndex = directoryIndex;
    }

    public StandardAnalyzer getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(StandardAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    public void buildIndex() {
        try {
            directoryIndex = FSDirectory.open(Paths.get(CommonValue.THESIS_PREPROCESS_PATH+"\\"+"rawindex.lucene"));
            analyzer = new StandardAnalyzer();
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
            IndexWriter writter = new IndexWriter(directoryIndex, indexWriterConfig);
            Document document;
            String title;
            StringBuilder body;

            MSWordFileHandler.getIns().loadAll();
            List<MSWordFile> msWordFiles = MSWordFileHandler.getIns().msWordFiles;

            for (MSWordFile msWordFile : msWordFiles) {
                document = new Document();
                title = msWordFile.getPath().substring(
                        msWordFile.getPath().lastIndexOf("\\") + 1);
                body = new StringBuilder();
                for (XWPFParagraph paragraph : msWordFile.getParagraphs()) {
                    body.append(paragraph.getText());
                }

                document.add(new TextField("title", title, Field.Store.YES));
                document.add(new TextField("body", body.toString(), Field.Store.YES));
                writter.addDocument(document);
            }
            writter.close();
        } catch (IOException ex) {
            Logger.getLogger(SearchingProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Document> searchIndex(String inField, String queryString) {
        try {
            Query query = new QueryParser(inField, analyzer)
                    .parse(queryString);

            IndexReader indexReader = DirectoryReader.open(directoryIndex);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10);
            List<Document> documents = new ArrayList<>();
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                documents.add(searcher.doc(scoreDoc.doc));
            }
            return documents;
        } catch (ParseException ex) {
            Logger.getLogger(SearchingProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SearchingProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        ConfigReader.getIns().read();
        SearchingProcessor sp = SearchingProcessor.getIns();
        sp.buildIndex();
        List<Document> documents = sp.searchIndex("body", "siêu âm");
        for (Document document : documents) {
            System.out.println(document.get("title"));
        }

    }
}
