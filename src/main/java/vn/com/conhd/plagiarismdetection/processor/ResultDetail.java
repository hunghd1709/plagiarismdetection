/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

import java.util.List;
import vn.com.conhd.plagiarismdetection.preprocessor.Sentence;

/**
 *
 * @author User
 */
public class ResultDetail {

    private Sentence firstSentence;
    private Sentence secondSentence;
    private double sim;
    private boolean isUsingSemantic;

    public ResultDetail() {
    }

    public ResultDetail(Sentence firstSentence, Sentence secondSentence, boolean isUsingSemantic) {
        this.firstSentence = firstSentence;
        this.secondSentence = secondSentence;
        this.isUsingSemantic = isUsingSemantic;
    }

    public ResultDetail(Sentence firstSentence, Sentence secondSentence, double sim) {
        this.firstSentence = firstSentence;
        this.secondSentence = secondSentence;
        this.sim = sim;
    }

    public Sentence getFirstSentence() {
        return firstSentence;
    }

    public void setFirstSentence(Sentence firstSentence) {
        this.firstSentence = firstSentence;
    }

    public Sentence getSecondSentence() {
        return secondSentence;
    }

    public void setSecondSentence(Sentence secondSentence) {
        this.secondSentence = secondSentence;
    }

    public double getSim() {
        return sim;
    }

    public void setSim(double sim) {
        this.sim = sim;
    }

    public boolean isIsUsingSemantic() {
        return isUsingSemantic;
    }

    public void setIsUsingSemantic(boolean isUsingSemantic) {
        this.isUsingSemantic = isUsingSemantic;
    }

    public void findBestMatch(List<Sentence> sentences) {
        double bestSim = Integer.MAX_VALUE;
        Sentence bestSentence = null;
        double sim;

        for (Sentence sentence : sentences) {
            if (isUsingSemantic) {
                sim = LevenshteinProcessor.getDistance(firstSentence.getFormattedContent(), sentence.getFormattedContent(), true);
            } else {
                sim = LevenshteinProcessor.getDistance(firstSentence.getFormattedContent(), sentence.getFormattedContent(), false);
            }
            if (sim < bestSim) {
                bestSim = sim;
                bestSentence = sentence;
            }

        }

        this.sim = bestSim;
        this.secondSentence = bestSentence;
    }
}
