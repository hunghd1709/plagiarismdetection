/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.poi.xwpf.converter.core.XWPFConverterException;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import vn.com.conhd.plagiarismdetection.config.CommonValue;
import vn.com.conhd.plagiarismdetection.config.ConfigReader;
import vn.com.conhd.plagiarismdetection.file.MSWordFile;
import vn.com.conhd.plagiarismdetection.file.MSWordFileHandler;
import vn.com.conhd.plagiarismdetection.preprocessor.Sentence;
import vn.com.conhd.plagiarismdetection.preprocessor.ThesisFile;

/**
 *
 * @author User
 */
public class Result implements Comparable<Result> {

    private ThesisFile firstThesis;
    private ThesisFile secondThesis;
    private double sim;
    private List<ResultDetail> resultDetails;
    private long time;
    private boolean isUsingSemantic;

    public Result() {
    }

    public Result(ThesisFile firstThesis, ThesisFile secondThesis) {
        this.firstThesis = firstThesis;
        this.secondThesis = secondThesis;
    }

    public Result(ThesisFile firstThesis, ThesisFile secondThesis, double sim) {
        this.firstThesis = firstThesis;
        this.secondThesis = secondThesis;
        this.sim = sim;
    }

    public ThesisFile getFirstThesis() {
        return firstThesis;
    }

    public void setFirstThesis(ThesisFile firstThesis) {
        this.firstThesis = firstThesis;
    }

    public ThesisFile getSecondThesis() {
        return secondThesis;
    }

    public void setSecondThesis(ThesisFile secondThesis) {
        this.secondThesis = secondThesis;
    }

    public double getSim() {
        return sim;
    }

    public void setSim(double sim) {
        this.sim = sim;
    }

    public List<ResultDetail> getResultDetails() {
        return resultDetails;
    }

    public void setResultDetails(List<ResultDetail> resultDetails) {
        this.resultDetails = resultDetails;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isIsUsingSemantic() {
        return isUsingSemantic;
    }

    public void setIsUsingSemantic(boolean isUsingSemantic) {
        this.isUsingSemantic = isUsingSemantic;
    }

    public void calculateSimilarity() {
        long start = new Date().getTime();
        resultDetails = new ArrayList<ResultDetail>();
        int count = 0;
        ResultDetail resultDetail;

        for (Sentence sentence : firstThesis.getSentences()) {
            resultDetail = new ResultDetail();
            resultDetail.setFirstSentence(sentence);
            resultDetail.setIsUsingSemantic(isUsingSemantic);
            resultDetail.findBestMatch(secondThesis.getSentences());
            if (resultDetail.getSim() < CommonValue.SENTENCE_CHANGE_THRESHOLE
                    && resultDetail.getSim() != -1.0) {
                count++;
            }
            resultDetails.add(resultDetail);
        }

        this.sim = count * 1.0 / firstThesis.getSentences().size();
        long end = new Date().getTime();
        time = end - start;
    }

    public void markResult() {
        String filename1 = this.firstThesis.getMsWordFile().getPath().substring(
                this.firstThesis.getMsWordFile().getPath().lastIndexOf("\\") + 1);
        filename1 = filename1.replace(".docx", ".pdf");
        
        String filename2 = this.secondThesis.getMsWordFile().getPath().substring(
                this.secondThesis.getMsWordFile().getPath().lastIndexOf("\\") + 1);
        filename2 = filename2.replace(".docx", ".pdf");

        List<ResultDetail> resultDetails = this.resultDetails.stream().filter(
                resultDetail -> resultDetail.getSim() < CommonValue.SENTENCE_CHANGE_THRESHOLE
                && resultDetail.getSim() != -1.0
                && !resultDetail.getFirstSentence().getRawContent().equals(" ")
                && !resultDetail.getSecondSentence().getRawContent().equals(" ")).collect(
                        Collectors.toList());
        XWPFDocument docx = firstThesis.getMsWordFile().getXdoc();

        for (ResultDetail resultDetail : resultDetails) {
            for (XWPFParagraph paragraph : docx.getParagraphs()) {
                for (XWPFRun run : paragraph.getRuns()) {
                    if (run.text().contains(resultDetail.getFirstSentence().getRawContent())) {
                        run.setColor("ff0000");
                    }
                }

            }
        }
        
        File directory = new File(CommonValue.SHOW_REPO_PATH + "/" + filename1.replaceAll(" ", "").replace(".pdf", "") +"_"+filename2.replaceAll(" ", "").replace(".pdf", ""));
        directory.mkdir();
        
        String path = CommonValue.SHOW_REPO_PATH + "/" + filename1.replaceAll(" ", "").replace(".pdf", "") +"_"+filename2.replaceAll(" ", "").replace(".pdf", "") + "/" + filename1;
        
        OutputStream out;
        try {
            out = new FileOutputStream(path);
            PdfOptions options = null;
            PdfConverter.getInstance().convert(docx, out, options);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XWPFConverterException | IOException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
        }
        docx = null;
        out = null;
        

        

//        resultDetails = this.resultDetails.stream().filter(
//                resultDetail -> resultDetail.getSim() < CommonValue.SENTENCE_CHANGE_THRESHOLE).collect(
//                        Collectors.toList());
        docx = secondThesis.getMsWordFile().getXdoc();

        for (ResultDetail resultDetail : resultDetails) {
            for (XWPFParagraph paragraph : docx.getParagraphs()) {
                for (XWPFRun run : paragraph.getRuns()) {
                    if (run.text().contains(resultDetail.getSecondSentence().getRawContent())) {
                        run.setColor("ff0000");
                    }
                }
            }
        }
        path = CommonValue.SHOW_REPO_PATH + "/" + filename1.replaceAll(" ", "").replace(".pdf", "") +"_"+filename2.replaceAll(" ", "").replace(".pdf", "") + "/" + filename2;
        try {
            out = new FileOutputStream(path);
            PdfOptions options = null;
            PdfConverter.getInstance().convert(docx, out, options);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XWPFConverterException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Result.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        docx = null;
        out = null;
        
        System.gc();

    }

    private int highlightSubsentence(String sentence, XWPFParagraph p, int i) {
        //get the current run Style - here I might need to save the current style
        XWPFRun currentRun = p.getRuns().get(i);
        String currentRunText = currentRun.text();
        int sentenceLength = sentence.length();
        int sentenceBeginIndex = currentRunText.indexOf(sentence);
        if (sentenceBeginIndex == -1) {
            return 0;
        }
        int addedRuns = 0;
        p.removeRun(i);
        //Create, if necessary, a run before the highlight part
        if (sentenceBeginIndex > 0) {
            XWPFRun before = p.insertNewRun(i);
            before.setText(currentRunText.substring(0, sentenceBeginIndex));
            //here I might need to re-introduce the style of the deleted run
            addedRuns++;
        }

        // highlight the interesting part
        XWPFRun sentenceRun = p.insertNewRun(i + addedRuns);
        sentenceRun.setText(currentRunText.substring(sentenceBeginIndex, sentenceBeginIndex + sentenceLength));
//        currentStyle.copyStyle(sentenceRun);
        CTShd cTShd = sentenceRun.getCTR().addNewRPr().addNewShd();
        cTShd.setFill("00FFFF");

        //Create, if necessary, a run after the highlight part
        if (sentenceBeginIndex + sentenceLength != currentRunText.length()) {
            XWPFRun after = p.insertNewRun(i + addedRuns + 1);
            after.setText(currentRunText.substring(sentenceBeginIndex + sentenceLength));
            //here I might need to re-introduce the style of the deleted run
            addedRuns++;
        }
        return addedRuns;
    }

    public static void main(String[] args) {
        ConfigReader.getIns().read();

        MSWordFileHandler msWordFileHandler = MSWordFileHandler.getIns();
        msWordFileHandler.loadAll();
        List<MSWordFile> msWordFiles = msWordFileHandler.getMsWordFiles();
        ThesisFile thesis;
        Result result;
        List<ThesisFile> these = new ArrayList<ThesisFile>();
        for (MSWordFile msWordFile : msWordFiles) {
            thesis = new ThesisFile(msWordFile);
            thesis.segment();
            these.add(thesis);
        }

        List<Result> results = new ArrayList<Result>();
        for (int i = 1; i < 2; i++) {
            System.out.println("Comparing " + i);
            result = new Result(these.get(0), these.get(i));
            result.calculateSimilarity();
            results.add(result);
        }

        result = results.get(0);
        for (int i = 1; i < results.size(); i++) {
            if (results.get(i).sim > result.sim) {
                result = results.get(i);
            }
        }

        result.markResult();

        System.out.println("");
    }

    @Override
    public int compareTo(Result o) {
        if (this.sim > o.getSim()) {
            return 1;
        } else if (this.sim == o.getSim()) {
            return 0;
        } else {
            return -1;
        }
    }
}
