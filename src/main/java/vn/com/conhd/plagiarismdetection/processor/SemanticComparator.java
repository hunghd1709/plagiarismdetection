/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.processor;

import java.util.HashMap;
import java.util.List;
import vn.com.conhd.plagiarismdetection.dao.SynonymDAO;
import vn.com.conhd.plagiarismdetection.dm.Synonym;

/**
 *
 * @author User
 */
public class SemanticComparator {

    private static SemanticComparator ins = null;
    public static HashMap<String, Synonym> hmSynonym = loadSynonym();

    private SemanticComparator() {

    }
    
    public static HashMap<String, Synonym> loadSynonym(){
        HashMap<String, Synonym> hmSynonym = new HashMap<>();
        List<Synonym> synonyms = SynonymDAO.getIns().readAll();
        System.out.println("Loading synonyms...");
        int i=0;
        for(Synonym synonym : synonyms){
            System.out.println(i++);
            hmSynonym.put(synonym.getContent(), synonym);
        }
        
        return hmSynonym;
    }

    public static SemanticComparator getIns() {
        if (ins == null) {
            ins = new SemanticComparator();
        }
        return ins;
    }

    public boolean isSynonym(String s, String t) {
        if (s.equals(t)) {
            return true;
        }

        Synonym s1 = hmSynonym.get(s);
        Synonym s2 = hmSynonym.get(t);

        if (s1 != null && s2 != null && s1.getConnotationID() == s2.getConnotationID()) {
            return true;
        }
        return false;
    }
}
