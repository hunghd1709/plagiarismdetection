/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.dm.Student;
import vn.com.conhd.plagiarismdetection.util.DBHelper;

/**
 *
 * @author User
 */
public class StudentDAO {

    private static StudentDAO ins = null;
    public static String TABLE_STUDENT = "students";
    public static String COLUMN_CODE = "code";
    public static String COLUMN_FIRSTNAME = "firstname";
    public static String COLUMN_SURNAME = "surname";
    public static String COLUMN_LASTNAME = "lastname";
    public static String COLUMN_YEAR = "year";
    public static String COLUMN_ACLASS = "aclass";
    public static String COLUMN_PHONENUMBER = "phonenumber";
    public static String COLUMN_EMAIL = "email";
    public static String COLUMN_DOB = "dob";
    public static String COLUMN_POB = "pob";

    private StudentDAO() {

    }

    public static StudentDAO getIns() {
        if (ins == null) {
            ins = new StudentDAO();
        }
        return ins;
    }

    public long insert(Student student) {
        String sql = "INSERT INTO " + TABLE_STUDENT + "("
                + COLUMN_CODE + ","
                + COLUMN_FIRSTNAME + ","
                + COLUMN_SURNAME + ","
                + COLUMN_LASTNAME + ","
                + COLUMN_YEAR
                + ") VALUES("
                + "'" + student.getCode() + "',"
                + "'" + student.getFirstname() + "',"
                + "'" + student.getSurname() + "',"
                + "'" + student.getLastname() + "',"
                + student.getYear() + ")";

        return DBHelper.getIns().executeUpdate(sql);
    }

    public List<Student> readAll() {
        List<Student> students = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_STUDENT;
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        try {
            while (rs.next()) {
                students.add(new Student(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        null,
                        null,
                        null,
                        null,
                        null));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return students;
    }

    public long delete(Student student) {
        String sql = "DELETE FROM " + TABLE_STUDENT
                + " WHERE " + COLUMN_CODE + "='" + student.getCode() + "'";
        return DBHelper.getIns().executeUpdate(sql);
    }

}
