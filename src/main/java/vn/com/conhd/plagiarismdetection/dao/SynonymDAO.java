/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.dm.Synonym;
import vn.com.conhd.plagiarismdetection.util.DBHelper;

/**
 *
 * @author User
 */
public class SynonymDAO {

    private static SynonymDAO ins = null;
    public static String TABLE_SYNONYM = "synonyms";
    public static String COLUMN_ID = "id";
    public static String COLUMN_CONNOTATION_ID = "connotation_id";
    public static String COLUMN_CONTENT = "content";

    private SynonymDAO() {

    }

    public static SynonymDAO getIns() {
        if (ins == null) {
            ins = new SynonymDAO();
        }
        return ins;
    }

    public long create(Synonym synonym) {
        String sql = "INSERT INTO " + TABLE_SYNONYM + " VALUES("
                + "NULL,"
                + synonym.getConnotationID() + ",'"
                + synonym.getContent() + "')";
        return DBHelper.getIns().executeUpdate(sql);
    }

    public List<Synonym> readAll() {
        List<Synonym> synonyms = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_SYNONYM;
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        if (rs == null) {
            return null;
        }

        try {
            while (rs.next()) {
                synonyms.add(new Synonym(rs.getInt(1), rs.getInt(2), rs.getString(3)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SynonymDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return synonyms;
    }

    public Synonym readByContent(String content) {
        String sql = "SELECT * FROM " + TABLE_SYNONYM
                + " WHERE " + COLUMN_CONTENT + "= '" + content + "'";
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        if (rs == null) {
            return null;
        }

        try {
            if (rs.next()) {
                return new Synonym(rs.getInt(1), rs.getInt(2), rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SynonymDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;
    }
    
    public long update(Synonym synonym){
        return 0;
    }
    
    public long delete(Synonym synonym){
        String sql = "DELETE FROM " + TABLE_SYNONYM + " WHERE "
                + COLUMN_ID + " = " + synonym.getId();
                
        return DBHelper.getIns().executeUpdate(sql);
    }

    public static void main(String[] args) {
//        List<Synonym> synonyms = SynonymDAO.getIns().readAll();
//        Synonym synonym = SynonymDAO.getIns().readByContent("");
//        System.out.println("");
    }
}
