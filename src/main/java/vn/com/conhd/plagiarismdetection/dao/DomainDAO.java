/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.dm.Domain;
import vn.com.conhd.plagiarismdetection.util.DBHelper;

/**
 *
 * @author User
 */
public class DomainDAO {

    private static DomainDAO ins = null;
    public static String TABLE_DOMAIN = "domains";
    public static String COLUMN_ID = "id";
    public static String COLUMN_NAME = "name";

    private DomainDAO() {

    }

    public static DomainDAO getIns() {
        if (ins == null) {
            ins = new DomainDAO();
        }
        return ins;
    }

    public long insert(Domain domain) {
        String sql = "INSERT INTO " + TABLE_DOMAIN + " VALUES("
                + "NULL,"
                + "'" + domain.getName()+ "')";
             
        return DBHelper.getIns().executeUpdate(sql);
    }

    public List<Domain> readAll() {
        List<Domain> domains = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_DOMAIN;
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        try {
            while (rs.next()) {
                domains.add(new Domain(rs.getInt(1),
                        rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DomainDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return domains;
    }

    public long delete(Domain domain) {
        String sql = "DELETE FROM " + TABLE_DOMAIN + " WHERE"
                + COLUMN_ID + " = " + domain.getId();
        return DBHelper.getIns().executeUpdate(sql);
    }
}
