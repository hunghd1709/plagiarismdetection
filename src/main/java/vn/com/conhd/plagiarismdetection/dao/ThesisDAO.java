/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.dm.Student;
import vn.com.conhd.plagiarismdetection.dm.Thesis;
import vn.com.conhd.plagiarismdetection.util.DBHelper;

/**
 *
 * @author User
 */
public class ThesisDAO {

    private static ThesisDAO ins = null;
    public static String TABLE_THESIS = "these";
    public static String COLUMN_CODE = "code";
    public static String COLUMN_DECISION_CODE = "decision_code";
    public static String COLUMN_STUDENT_CODE = "student_code";
    public static String COLUMN_DOMAIN_ID = "domain_id";
    public static String COLUMN_YEAR = "year";
    public static String COLUMN_TITLE = "title";
    public static String COLUMN_SUMMARY = "summary";

    private ThesisDAO() {

    }

    public static ThesisDAO getIns() {
        if (ins == null) {
            ins = new ThesisDAO();
        }
        return ins;
    }

    public long insert(Thesis thesis) {
        String sql = "INSERT INTO " + TABLE_THESIS + " VALUES("
                + "'" + thesis.getCode() + "',"
                + "'" + thesis.getDecisionCode() + "',"
                + "'" + thesis.getStudentCode() + "',"
                + thesis.getDomainID() + ","
                + thesis.getYear() + ","
                + "'" + thesis.getTitle() + "',"
                + "'" + thesis.getSummary() + "')";

        return DBHelper.getIns().executeUpdate(sql);
    }

    public List<Thesis> readAll() {
        List<Thesis> these = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_THESIS;
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        try {
            while (rs.next()) {
                these.add(new Thesis(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ThesisDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return these;
    }

    public long delete(Thesis thesis) {
        String sql = "DELETE FROM " + TABLE_THESIS
                + " WHERE " + COLUMN_CODE + "='" + thesis.getCode() + "'";
        return DBHelper.getIns().executeUpdate(sql);
    }

}
