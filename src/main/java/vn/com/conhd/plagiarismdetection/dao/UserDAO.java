/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.dm.User;
import vn.com.conhd.plagiarismdetection.util.DBHelper;

/**
 *
 * @author User
 */
public class UserDAO {

    private static UserDAO ins = null;
    public static String TABLE_USER = "users";
    public static String COLUMN_ID = "id";
    public static String COLUMN_USERNAME = "username";
    public static String COLUMN_PASSWORD = "password";
    public static String COLUMN_STATUS = "status";
    public static String COLUMN_EMAIL = "email";
    public static String COLUMN_PHONE = "phone";

    private UserDAO() {

    }

    public static UserDAO getIns() {
        if (ins == null) {
            ins = new UserDAO();
        }
        return ins;
    }

    public boolean checkLogin(String username, String password) {
        String sql = "SELECT * FROM " + TABLE_USER + " WHERE "
                + COLUMN_USERNAME + " = '" + username + "' AND "
                + COLUMN_PASSWORD + " = '" + password + "'";
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        try {
            return rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }

    public long insert(User user) {
        String sql = "INSERT INTO " + TABLE_USER + " VALUES("
                + "NULL,"
                + "'" + user.getUsername() + "',"
                + "'" + user.getPassword() + "',"
                + user.getStatus() +","
                + "'" + user.getEmail() + "',"
                + "'" + user.getPhone() + "')";
        return DBHelper.getIns().executeUpdate(sql);
    }

    public List<User> readAll() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_USER;
        ResultSet rs = DBHelper.getIns().executeQuery(sql);
        try {
            while (rs.next()) {
                users.add(new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return users;
    }

    public long delete(User user) {
        String sql = "DELETE FROM " + TABLE_USER + " WHERE"
                + COLUMN_ID + " = " + user.getId();
        return DBHelper.getIns().executeUpdate(sql);
    }
}
