/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.tm;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import vn.com.conhd.plagiarismdetection.dm.Student;

/**
 *
 * @author User
 */
public class StudentTableModel extends AbstractTableModel{
    
    private List<Student> students;

    private final String[] columnNames = new String[]{
        "Code", "Firstname", "Surname", "Lastname", "Year"
    };
    private final Class[] columnClass = new Class[]{
        String.class, String.class, String.class, String.class, Integer.class
    };

    public StudentTableModel() {
    }

    public StudentTableModel(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return students.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Student row = students.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return row.getCode();
            case 1:
                return row.getFirstname();
            case 2:
                return row.getSurname();
            case 3:
                return row.getLastname();
            case 4:
                return row.getYear();
            default:
                break;
        }
        return null;
    }
}
