/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.tm;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import vn.com.conhd.plagiarismdetection.dm.Thesis;

/**
 *
 * @author User
 */
public class ThesisTableModel extends AbstractTableModel{
    private List<Thesis> these;

    private final String[] columnNames = new String[]{
        "Code", "Decision Code", "Student Code", "Domain ID", "Year", "Title", "Summary"
    };
    private final Class[] columnClass = new Class[]{
        String.class, String.class, String.class, String.class, Integer.class, String.class, String.class
    };

    public ThesisTableModel(List<Thesis> these) {
        this.these = these;
    }

    public List<Thesis> getThese() {
        return these;
    }

    public void setThese(List<Thesis> these) {
        this.these = these;
    }
    
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return these.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Thesis row = these.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return row.getCode();
            case 1:
                return row.getDecisionCode();
            case 2:
                return row.getStudentCode();
            case 3:
                return row.getDomainID();
            case 4:
                return row.getYear();
            case 5:
                return row.getTitle();
            case 6:
                return row.getSummary();
            default:
                break;
        }
        return null;
    }
}
