/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.tm;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import vn.com.conhd.plagiarismdetection.dm.User;

/**
 *
 * @author User
 */
public class UserTableModel extends AbstractTableModel {

    private List<User> users;

    private final String[] columnNames = new String[]{
        "Id", "Username", "Password", "Status", "Email", "Phone"
    };
    private final Class[] columnClass = new Class[]{
        Integer.class, String.class, String.class, Integer.class, String.class, String.class
    };

    public UserTableModel(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User row = users.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return row.getId();
            case 1:
                return row.getUsername();
            case 2:
                return "*******";
            case 3:
                return row.getStatus();
            case 4:
                return row.getEmail();
            case 5:
                return row.getPhone();
            default:
                break;
        }
        return null;
    }
}
