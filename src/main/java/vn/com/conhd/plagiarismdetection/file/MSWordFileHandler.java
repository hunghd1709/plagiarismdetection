/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import vn.com.conhd.plagiarismdetection.config.CommonValue;
import vn.com.conhd.plagiarismdetection.config.ConfigReader;
import vn.com.conhd.plagiarismdetection.ui.FormMain;

/**
 *
 * @author User
 */
public class MSWordFileHandler {

    private static MSWordFileHandler ins = null;
    public List<MSWordFile> msWordFiles = null;

    private MSWordFileHandler() {

    }

    public List<MSWordFile> getMsWordFiles() {
        return msWordFiles;
    }

    public void setMsWordFiles(List<MSWordFile> msWordFiles) {
        this.msWordFiles = msWordFiles;
    }

    public static MSWordFileHandler getIns() {
        if (ins == null) {
            ins = new MSWordFileHandler();
        }

        return ins;
    }

//    public MSWordFile loafFile(String path) {
//        MSWordFile msWordFile = new MSWordFile(path);
//        msWordFile.load();
//        return msWordFile;
//    }

    public void listFile(String directoryName, List<MSWordFile> files) {
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        if (fList != null) {
            for (File file : fList) {
                if (file.isFile() && (FilenameUtils.getExtension(file.getAbsolutePath()).equals("docx")
                        || FilenameUtils.getExtension(file.getAbsolutePath()).equals("doc"))) {
                    FormMain.getIns().addLog(file.getAbsolutePath());
                    MSWordFile mSWordFile = new MSWordFile(file.getAbsolutePath());
                    mSWordFile.load();
                    files.add(mSWordFile);
                } else if (file.isDirectory()) {
                    listFile(file.getAbsolutePath(), files);
                }
            }
        }
    }

    public void loadAll() {
        msWordFiles = new ArrayList<>();
        listFile(CommonValue.THESIS_REPO_PATH, msWordFiles);
    }

    public List<MSWordFile> loadDir(String directoryName) {
        List<MSWordFile> msWordFiles = new ArrayList<>();
        listFile(directoryName, msWordFiles);
        return msWordFiles;
    }

    public MSWordFile loadFile(String path) {
        MSWordFile msWordFile = new MSWordFile(path);
        msWordFile.load();
        return msWordFile;
    }

    public static void main(String[] args) {
        ConfigReader configReader = ConfigReader.getIns();
        configReader.read();

        MSWordFileHandler mswfh = getIns();
        mswfh.loadAll();
        
        for(MSWordFile msWordFile : mswfh.getMsWordFiles()){
            msWordFile.formatDocument();
            msWordFile.saveDocument(msWordFile.getPath().replace(".docx", "_1.docx"));
        }
    }
}
