/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;

/**
 *
 * @author User
 */
public class MSWordFile {
    
    String path;
    XWPFDocument xdoc;
    List<XWPFParagraph> paragraphs;
    List<XWPFTable> tables;
    
    public MSWordFile() {
        paragraphs = new ArrayList<>();
        tables = new ArrayList<>();
    }
    
    public MSWordFile(String path) {
        this.path = path;
    }
    
    public MSWordFile(String path, List<XWPFParagraph> paragraphs, List<XWPFTable> tables) {
        this.path = path;
        this.paragraphs = paragraphs;
        this.tables = tables;
    }
    
    public String getPath() {
        return path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
    
    public List<XWPFParagraph> getParagraphs() {
        return paragraphs;
    }
    
    public void setParagraphs(List<XWPFParagraph> paragraphs) {
        this.paragraphs = paragraphs;
    }
    
    public List<XWPFTable> getTables() {
        return tables;
    }
    
    public void setTables(List<XWPFTable> tables) {
        this.tables = tables;
    }
    
    public XWPFDocument getXdoc() {
        return xdoc;
    }
    
    public void setXdoc(XWPFDocument xdoc) {
        this.xdoc = xdoc;
    }
    
    public void load() {
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            xdoc = new XWPFDocument(OPCPackage.open(fileInputStream));
            paragraphs = xdoc.getParagraphs();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MSWordFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MSWordFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            Logger.getLogger(MSWordFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveDocument(String path) {
        try (FileOutputStream out = new FileOutputStream(path)) {
            xdoc.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void formatDocument() {
        List<XWPFParagraph> ps = xdoc.getParagraphs();
        String text;
        String[] ss;
        XWPFRun run;
        for (XWPFParagraph p : ps) {
            text = p.getText();
            text = text.trim();
            text = text.replace("  ", " ");
            ss = text.split("\\.");
            
            for (XWPFRun r : p.getRuns()) {
                r.setText("", 0);
            }

            for(String s : ss){
                run = p.createRun();
                run.setText(s+".");
            }
        }
        
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (XWPFParagraph paragraph : paragraphs) {
            sb.append(paragraph.getText());
            sb.append("\n");
        }
        return sb.toString();
    }
    
}
