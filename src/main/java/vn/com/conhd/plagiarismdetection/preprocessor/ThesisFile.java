/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.preprocessor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import vn.com.conhd.plagiarismdetection.config.CommonValue;
import vn.com.conhd.plagiarismdetection.config.ConfigReader;
import vn.com.conhd.plagiarismdetection.file.MSWordFile;
import vn.com.conhd.plagiarismdetection.file.MSWordFileHandler;
import vn.com.conhd.plagiarismdetection.wordsegmenter.WordSegmenter;

/**
 *
 * @author User
 */
public class ThesisFile{

    private MSWordFile msWordFile;
    private List<Sentence> sentences;

    public ThesisFile() {
    }

    public ThesisFile(MSWordFile msWordFile) {
        this.msWordFile = msWordFile;
    }

    public MSWordFile getMsWordFile() {
        return msWordFile;
    }

    public void setMsWordFile(MSWordFile msWordFile) {
        this.msWordFile = msWordFile;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public void segment() {
        Sentence sentence;
        this.sentences = new ArrayList<Sentence>();
        String s;

        for (XWPFParagraph paragraph : msWordFile.getParagraphs()) {
            String sss = paragraph.getText();
            sss = sss.trim();
            sss = sss.replace("...", ".");
            
            String[] sentences = sss.split("\\.");
            for (String bs : sentences) {
                bs = bs.trim();
                s = bs;
                if (s.length() > 3 && s.substring(s.length() - 3, s.length() - 1).equals("...")) {
                    s = s.substring(0, s.length() - 4);
                }
                if (s.length() > 1 && s.subSequence(s.length() - 2, s.length() - 1).equals("\\.|\\?|\\!")) {
                    s = s.substring(0, s.length() - 2);
                }
                s = s.toLowerCase();

                while (s.contains("..")) {
                    s = s.replaceAll("..", "");
                }

                if (s.equals("") || s.equals(".") || s.equals("\n") || s.length() == 0) {
                    continue;
                }

                if (/*Character.isDigit(s.charAt(0)) ||*/ Character.isDigit(s.charAt(s.length() - 1))) {
                    continue;
                }

                if (s.startsWith("(") || s.startsWith("hình")) {
                    continue;
                }

                if (s.startsWith("i.")
                        || s.startsWith("ii.")
                        || s.startsWith("iii.")
                        || s.startsWith("iv.")
                        || s.startsWith("v.")
                        || s.startsWith("vi.")
                        || s.startsWith("vii.")
                        || s.startsWith("viii.")
                        || s.startsWith("ix.")) {
                    continue;
                }

                s = s.replaceAll("\\-", "");
                s = s.replaceAll("\\+", "");
                s = s.replaceAll("\\.", "");
                s = s.replaceAll("\\*", "");
                s = s.replaceAll("…", "");
                //   s = s.replaceAll("\n", "");

                if (s.equals("") || s.equals(".") || s.equals("\n") || s.length() == 0) {
                    continue;
                }
                sentence = new Sentence(bs, s);
                sentence.segment();
                this.sentences.add(sentence);
            }
        }
    }

    public void write() {
        try {
            WordSegmenter wordSegmenter = WordSegmenter.getIns();
            String path = CommonValue.DEFAULT_THESIS_PREPROCESS_PATH
                    + "/" + this.msWordFile.getPath()
                    .substring(this.msWordFile.getPath().lastIndexOf("\\") + 1);
            path = path.replace(".docx", ".txt");
            path = path.replace(".doc", ".txt");
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(path),
                    StandardCharsets.UTF_8);
            for (Sentence sentence : sentences) {
//                osw.write(sentence.getRawContent());
//                osw.write("\n");
                osw.write(wordSegmenter.segmentTokenizedString(sentence.getFormattedContent()));
                osw.write("\n");
            }
            osw.close();
        } catch (IOException ex) {
            Logger.getLogger(WordSegmenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        ConfigReader.getIns().read();

        MSWordFileHandler msWordFileHandler = MSWordFileHandler.getIns();
        msWordFileHandler.loadAll();
        for (MSWordFile msWordFile : msWordFileHandler.getMsWordFiles()) {
            ThesisFile thesis = new ThesisFile(msWordFile);
            thesis.segment();
            thesis.write();
        }
        
        
    }
}
