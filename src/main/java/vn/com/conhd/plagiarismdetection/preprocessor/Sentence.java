/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.preprocessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.com.conhd.plagiarismdetection.wordsegmenter.WordSegmenter;

/**
 *
 * @author User
 */
public class Sentence {

    private String rawContent = null;
    private String formattedContent = null;
    private List<String> terms = null;

    public Sentence() {
    }

    public Sentence(String rawContent, String formattedContent){
        this.rawContent = rawContent;
        this.formattedContent = formattedContent;
    }

    public String getRawContent() {
        return rawContent;
    }

    public void setRawContent(String rawContent) {
        this.rawContent = rawContent;
    }

    public String getFormattedContent() {
        return formattedContent;
    }

    public void setFormattedContent(String formattedContent) {
        this.formattedContent = formattedContent;
    }

    

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public void segment() {
        WordSegmenter wordSegmenter = WordSegmenter.getIns();
        try {
            formattedContent = wordSegmenter.segmentTokenizedString(formattedContent);
        } catch (IOException ex) {
            Logger.getLogger(Sentence.class.getName()).log(Level.SEVERE, null, ex);
        }
        terms = new ArrayList<String>();
        String[] split = formattedContent.split(" ");
        for (String s : split) {
            terms.add(s);
        }
    }
}
