/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class DBHelper {

    private static DBHelper ins = null;
    private Connection conn;
    private String dbName = "plagiarism_detection";
    private String host = "localhost";
    private int port = 3306;
    private String username = "root";
    private String password = "";

    private DBHelper() {
        openConn();
    }

    public void openConn() {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbName,
                    username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn() {
        try {
            conn.close();
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static DBHelper getIns() {
        if (ins == null) {
            ins = new DBHelper();
        }
        return ins;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long executeUpdate(String sql) {
        if (conn == null) {
            return -1;
        }
        try {
            Statement stm = conn.createStatement();
            return stm.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public ResultSet executeQuery(String sql) {
        if (conn == null) {
            return null;
        }
        try {
            Statement stm = conn.createStatement();
            return stm.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public boolean executeSQL(String sql) {
        return true;
    }
}
