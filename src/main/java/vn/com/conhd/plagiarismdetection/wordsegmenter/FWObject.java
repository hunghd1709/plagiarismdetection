/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.wordsegmenter;

/**
 *
 * @author User
 */
public class FWObject {

    private String[] context;

    public FWObject(boolean check) {
        context = new String[10];
        if (check == true) {
            for (int i = 0; i < 10; i += 2) {
                context[i] = "<W>";
                context[i + 1] = "<T>";
            }
        }
    }

    public String[] getContext() {
        return context;
    }

    public void setContext(String[] context) {
        this.context = context;
    }
}
