/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.com.conhd.plagiarismdetection.wordsegmenter;

/**
 *
 * @author User
 */
public class WordTag {

    public String word;
    public String tag;
    public String form;

    public WordTag(String iword, String itag) {
        form = iword;
        word = iword.toLowerCase();
        tag = itag;
    }
}
